# Notes, questions, remarques site bemaker.eu

## page d'accueil:

* plus d'une image dans le banner hero principal? Montrer des ateliers en cours, photos des trucs, projets
* adapter le language: "embarque a bord tu fabbus" → fab truck? tech truck? qu'on s'accorde la dessus?
* si il n'y a pas d'elements qui populent les sections, doivent-elles s'afficher? Les arrets du fabbus? Ateliers? Si oui, alors montrons des evenements dans le passe ainsi que des evenements dans le futur?
* clarifier la difference de language entre Tutoriels et Projets? Je crois que les mots sont justes c'est simplement le manque d'une petite ligne explicative: BeMaker peut t'initier a plusieurs axes des pratiques des Fablabs, ici des tutoriels pour aller plus loin. Ou "Ci dessous des projets qui mettent en pratique tout ce que vous avez apris via les tutoriels." et qu'on renvoi des rappels les uns aux autres en interne.
* footer: bien massif pour ce qu'il contient, il semble qu'il devait recevoir d'autres elements? Plus de partenaires?
* les liens instagram et FB n'envoient a rien
* question generale: la homepage reflete un morceau de chaque page presente sur le site, mais pas la page ecosysteme par exemple, s'impose t'on que la home page est une vitrine pour le site? Ou doit elle plutot servir a presenter le projet bemaker plus precisement? Cette question pointe l'utilite du menu qui est toujours cache.
* menu: foireux que le titre de la page qui est visitee a l'instant se change de couleur pour devenir invisible.

## page tutoriels https://www.bemaker.eu/tutoriels/ & page projets

* manque un texte descriptif des ambitions des tutos et des projets

## page ecosysteme:

* elle est bien cette page, mais il manque aussi un peu de texte contextuel, simplement expliquer pourquoi cette page se nomme ecosysteme me semblerai un bon depart.
* ailleurs, cette page ne se retrouve pas referencee sur la home, quelle est la fonction de la homepage?

## page interne tutoriel

* status du badge? Il sert a qui? Juste pour l'etudiant? Juste pour le prof? Est-ce pour nous?
* navigation: bread crumb pour signifier au sein de quel lecon on se trouve, nagiguer d'un bout a l'autre des lecons.

## remarques visuelles:
ref https://gitlab.com/bemaker/graphics.collection
* rester en roboto et hershey fonts, c'est tout, pas de script fancy.
* faire attention aux univers graphiques dans les tutos, on a deja pas mal de different types d'images qui proviennent de Fritzing etc, faut pas tout melanger.
