# Arborescence **bemaker.eu**

## État courrant:

* homepage vitrine
  * image hero
  * “Embarque à bord du fabbus” → sorte de about ?
  * les arrets du fabbus (retro carto? — vide)
  * ateliers (références vers activités partenaires — vide)
  * tutoriels (contenu educatif de base, stable, chapitré et fournis)
  * projets (compétences appliquées)
* navigation collapsed
  * tutoriels
  * ateliers
  * projets
  * écosystème
  * contact
  * connection
  * inscription
  * FR
  * NL

Conclusions etat courrant:
navigation complexe, beaucoup de sections vides, confusion possible entre ateliers, projets et tutoriels, navigation cachée, bonne home page vitrine, mais plusieurs sections vides.
Manque multiples textes d'introduction au projet BeMaker, aux idées écosystème, la page contact est assez enfuie.

---

## proposition d'Arborescence neuve:

* navigation principale:
  * À propos + écosystème + reférence à la page contact
  * Apprendre; tutoriels / projets (nouvelle page qui combine les deux)
  * Participer; ateliers / workshops / evenements partenaires (!! section visible si des items y existent)
  * contact (équipe + formulaire de contact + infos contact etc)
* navigation secondaire:
  * Log in / sign up
  * lang switcher FR/NL
  * newsletter
* footer
  * partenaires
  * reseaux sociaux
  * contact
  * nous soutenir ?

Les 5 éléments de navigation primaire seraient visibles déployés en liste horizontale, la navigation secondaire serait un élément tiers à placer soit au sein de la navigation principale mais contrastée, soit dans un autre coin de l'écran.

* home page: à disctuter en équipe
  * slider image hero, caroussel avec multiples images, commencant par le truck, puis des images de groupes avec enfants, une bonne sélection d'images
  * à propos déployé, quelques lignes décrivant BeMaker et son projet. Bouton “Lire plus” / “en savoir plus” pour accéder à la page complete incluant plus de texte, plus d'images, l'écosystème.
  * Tutoriels: reutiliser cette section existante, manque simplement une ligne de texte pour expliquer ce que nous entendons par Tutos, contenu statique
  * projets: idem, reutiliser section existante + texte explicatif.
  * !! notez le changement possible de gestion de ces fichiers; flatfile cms, export to CMS, voir comment intégrer ceci dans le site.
  * Ateliers: reutiliser existant, mais que montrer cette section si il y a des articles dedans.
  * contact: nouvelle section à créer, un formulaire de contact disponible dès la page d'acceuil.

  ---
   ## notes 13.06

* tutoriels et projets dans la meme page ? Un macaron pour les tutos vias lesquels on peut gagner des badges.
* Gestion des tutoriels; une page accordeon, plutot que multiples pages
